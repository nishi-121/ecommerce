<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="com.nishi.DAO.StudentOperation" %>
    <%@page import="com.nishi.DAO.SubjectOperation" %>
    <%@page import="com.nishi.Bean.Subject" %>
    <%@page import="java.util.List" %>
<%@page import="java.util.Set" %>
<%@page import="java.util.ListIterator" %>
<%@page import="java.util.Iterator" %>
    <%@page import="com.nishi.Bean.Student" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Updation page</title>
</head>
<body>
<%
if(request.getParameter("id")!=null){
int id=Integer.parseInt(request.getParameter("id"));
StudentOperation st=new StudentOperation();
Student student=st.selectStudent(id);
if(student!=null){
%>
<form action="finalUpdate.jsp" method="post">
<input type="hidden" value="<%= student.getId() %>" name="id">
<label for="name">Name:</label>
<input type="text" value="<%= student.getName() %>" name="name">
<label for="name">Password:</label>
<input type="password" value="<%= student.getPassword() %>" name="password">
<label for="address">Address:</label>
<input type="text" value="<%= student.getAddress() %>" name="address">
<label for="course"></label>
<input type="text" value="<%= student.getCourse() %>" name="course">
<label for="submit"></label>
<input type="submit" value="go">
</form>

<%}}
else{
	out.println("select any recors cause id is null");
}
%>

<div>
<h3>Taken by student</h3>
<form action="RemoveSubjectFromStudent">
<%
StudentOperation st2 =new StudentOperation();
int id=Integer.parseInt(request.getParameter("id"));
Student student2=st2.selectStudent(id);
Set s=student2.getSubjectList();
Iterator ite=s.iterator();
int i=0;
while(ite.hasNext())
{
	i++;
Subject	sub2 = (Subject)ite.next();	
%>
<li>
<input type="checkbox" name="<%=sub2.getSub_name() %>" value="<%=sub2.getSub_id() %>">&nbsp;&nbsp;&nbsp;&nbsp;<%=sub2.getSub_name()%>
</li>
<%
}
%>
<input type="hidden" name="id" value="<%=id%>">
<button type="submit" name="removesub" class="btn btn-block btn-success" value="Add">Remove subject from student</button>
</form>
</div>

<div>
<h3>All avilable subject</h3>
<form action="AddSubjectControll">
<%
SubjectOperation subo = new SubjectOperation();
List li= subo.selectAllSubject();
ListIterator lit=li.listIterator();
while(lit.hasNext())
{
Subject	sub = (Subject)lit.next();
%>
<li>
<input type="checkbox" name="<%=sub.getSub_name() %>" value="<%=sub.getSub_id() %>">&nbsp;&nbsp;&nbsp;&nbsp;<%=sub.getSub_name()%>
</li>
<%
}
%>
<input type="hidden" name="id" value="<%=id%>">
<button type="submit" name="removesub" class="btn btn-block btn-success" value="Add">Add subject to the student</button>
</form>
</div>

</body>
</html>