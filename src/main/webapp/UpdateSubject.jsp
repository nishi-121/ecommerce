<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="com.nishi.DAO.SubjectOperation" %>
    <%@page import="com.nishi.Bean.Subject" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>UpdateStudent page</title>
<style>
.form{
        width: 100%; 
        margin: 8px 0;
        padding: 12px 20px; 
        display: inline-block;         
        box-sizing: border-box;
}
</style>
</head>
<body>
<%
if(request.getParameter("id")!=null){
int sub_id=Integer.parseInt(request.getParameter("sub_id"));
SubjectOperation st=new SubjectOperation();
Subject subject=st.selectSubject(sub_id);
if(subject!=null){
%>
<form action="finalUpdateSubject.jsp" method="post">
<input type="hidden" value="<%= subject.getSub_id() %>" name="id">

<label for="sub_name">Subject Name:</label>
<input type="text" value="<%= subject.getSub_name() %>" name="sub_name">

<label for="sub_max_marks">Subject Max Mark:</label>
<input type="text" value="<%= subject.getSub_max_marks() %>" name="sub_max_marks">

<label for="sub_passing_marks">Subject Passing Mark:</label>
<input type="text" value="<%=subject.getSub_passing_marks() %>" name="sub_passing_marks">

<label for="sub_fees">Subject Fees:</label>
<input type="text" value="<%=subject.getSub_fees() %>" name="sub_fees">

<label for="sub_total_question">Subject Total Question:</label>
<input type="text" value="<%=subject.getSub_total_questions() %>" name="sub_total_question">

<label for="sub_offer">Subject Offer:</label>
<input type="text" value="<%=subject.getSub_offer() %>" name="sub_offer">

<label for="sub_duration">Subject Duration:</label>
<input type="text" value="<%=subject.getSub_duration() %>" name="sub_duration">

<label for="submit"></label>
<input type="submit" value="go">
</form>

<%}}
else{
	out.println("select any recors cause id is null");
}
%>

</body>
</html>




