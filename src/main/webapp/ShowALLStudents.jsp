<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="com.nishi.DAO.StudentOperation" %>
<%@page import="java.util.List" %>
<%@page import="java.util.Set" %>
<%@page import="java.util.ListIterator" %>
<%@page import="java.util.Iterator" %>
<%@page import="com.nishi.Bean.Student" %>
<%@page import="com.nishi.Bean.Subject" %>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style>
* {box-sizing: border-box;}

body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.topnav {
  overflow: hidden;
  background-color: #e9e9e9;
}

.topnav a {
  float: left;
  display: block;
  color: black;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: #2196F3;
  color: white;
}

.topnav .search-container {
  float: right;
}

.topnav input[type=text] {
  padding: 6px;
  margin-top: 8px;
  font-size: 17px;
  border: none;
}

.topnav .search-container button {
  float: right;
  padding: 6px 10px;
  margin-top: 8px;
  margin-right: 16px;
  background: #ddd;
  font-size: 17px;
  border: none;
  cursor: pointer;
}

.topnav .search-container button:hover {
  background: #ccc;
}

@media screen and (max-width: 600px) {
  .topnav .search-container {
    float: none;
  }
  .topnav a, .topnav input[type=text], .topnav .search-container button {
    float: none;
    display: block;
    text-align: left;
    width: 100%;
    margin: 0;
    padding: 14px;
  }
  .topnav input[type=text] {
    border: 1px solid #ccc;  
  }
}

.update,.delete{
border-radius:10px;
padding:5px;
}
.update{
border:thin solid green;
}
.delete{
border:thin solid red;
}
td{
border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}
</style>
</head>
<body>
<div class="topnav">
  <a class="active" href="#home">Home</a>
  <a href="#about">About</a>
  <a href="#contact">Contact</a>
  <div class="search-container">
    <form action="#">
      <input type="text" placeholder="Search.." name="search">
      <button name="submit_search" type="submit"><i class="fa fa-search"></i></button>
    </form>
  </div>
</div>
<%
if(request.getParameter("submit_search")!=null){
	String targetString=request.getParameter("search");
%>

<div class="container">
<table class="table table-striped">
<thead>
<tr >
<th>ID</th>
<th>name</th>
<th>password</th>
<th>address</th>
<th>course</th>
<th>course_detail</th>
</tr>
</thead>
<%
StudentOperation st = new StudentOperation();
List li = st.searchByName(targetString);
ListIterator lit = li.listIterator();
while(lit.hasNext()){
Student student = (Student) lit.next();
%>
<tr>
<td><%= student.getId() %></td>
<td><%= student.getName() %></td>
<td><%= student.getPassword() %></td>
<td><%= student.getAddress() %></td>
<td><%= student.getCourse() %></td>
<td>
<%
Set s=student.getSubjectList();
Iterator ite=s.iterator();
while(ite.hasNext()){
	Subject sub2 = (Subject)ite.next();
	out.println(sub2.getSub_name());
}
%>
</td>
<td><a class="update" href="Update.jsp?id=<%= student.getId() %>">Update</a></td>
<td><a class="delete" href="Delete.jsp?id=<%= student.getId() %>">Delete</a></td>
</tr>
<% 
}
%>
</table>
</div>
<hr>
<%
}
%>

<!-- -----------comment -->
<div class="container">
<table class="table table-striped">
<thead>
<tr >
<th>ID</th>
<th>name</th>
<th>password</th>
<th>address</th>
<th>course</th>
<th>course_detail</th>
</tr>
</thead>
<%
StudentOperation st = new StudentOperation();
List li = st.selectAllStudent();
ListIterator lit = li.listIterator();
while(lit.hasNext()){
Student student = (Student) lit.next();
%>
<tr>
<td><%= student.getId() %></td>
<td><%= student.getName() %></td>
<td><%= student.getPassword() %></td>
<td><%= student.getAddress() %></td>
<td><%= student.getCourse() %></td>

<td>
<%
Set s=student.getSubjectList();
Iterator ite=s.iterator();
while(ite.hasNext()){
	Subject sub2 = (Subject)ite.next();
	out.println(sub2.getSub_name());
}
%>
</td>
<td><a class="update" href="Update.jsp?id=<%= student.getId() %>">Update</a></td>
<td><a class="delete" href="Delete.jsp?id=<%= student.getId() %>">Delete</a></td>
</tr>
<% 
}
%>
</table>
</div>
</body>
</html>