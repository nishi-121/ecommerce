<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style>
.form{
        width: 100%; 
        margin: 8px 0;
        padding: 12px 20px; 
        display: inline-block;         
        box-sizing: border-box;
}
</style>
</head>
<body>
<form action="AddSubjectController">
<label for="sub_name">Subject Name:</label>
<input type="text" class="form" name="sub_name">

<label for="sub_max_marks">Subject Max Mark:</label>
<input type="text" class="form" name="sub_max_marks">

<label for="sub_passing_marks">Subject Passing Mark:</label>
<input type="text" class="form" name="sub_passing_marks">

<label for="sub_fees">Subject Fees:</label>
<input type="text" class="form" name="sub_fees">

<label for="sub_total_question">Subject Total Question:</label>
<input type="text" class="form" name="sub_total_question">

<label for="sub_duration">Subject Duration:</label>
<input type="text" class="form" name="sub_duration">

<label for="sub_offer">Subject Offer:</label>
<input type="text" class="form" name="sub_offer">

<input type="submit" value="go">
</form>
</body>
</html>