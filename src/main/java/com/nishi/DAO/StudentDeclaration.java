package com.nishi.DAO;

import java.util.List;

import com.nishi.Bean.Student;

public interface StudentDeclaration {
	public int insertStudent(Student st);

	public int upDate(int id, Student st);

	public boolean delete(int id);

	public Student selectStudent(int id);

	public List selectAllStudent();

	public List searchByName(String name);

}
