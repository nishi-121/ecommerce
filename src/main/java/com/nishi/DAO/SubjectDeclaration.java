package com.nishi.DAO;

import java.util.List;

import com.nishi.Bean.Subject;

public interface SubjectDeclaration {
	public int insertSubject(Subject sub);

	public int upDate(int sub_id, Subject sub);

	public boolean delete(int sub_id);

	public Subject selectSubject(int sub_id);

	public List selectAllSubject();

	public List searchByName(String name);

}
