package com.nishi.DAO;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Restrictions;

import com.nishi.Bean.Student;
import com.nishi.HelpingClasses.SessionFact;

public class StudentOperation implements StudentDeclaration {

	SessionFactory sf;
	Session ss;
	Transaction tr;

	public StudentOperation() {
		sf = SessionFact.getSessionFact();
		ss = sf.openSession();
	}

	@Override
	public int insertStudent(Student st) {
		tr = ss.beginTransaction();
		System.err.println("" + st.getName() + st.getAddress());
		int l = (Integer) ss.save(st);
		tr.commit();
		if (l > 0) {
			return l;
		} else {
			return 0;
		}
	}

	public int upDate(int id, Student st) {
		tr = ss.beginTransaction();
		st.setId(id);
		try {
			ss.update(st);
			tr.commit();
			return id;
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return 0;
	}

	public boolean delete(int id) {
		tr = ss.beginTransaction();
		Student st = new Student();
		st.setId(id);
		try {
			ss.delete(st);
			tr.commit();
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return false;
	}

	public Student selectStudent(int id) {
		Student st = (Student) ss.get(Student.class, id);
		if (st != null) {
			return st;
		}
		return null;
	}

	@Override
	public List selectAllStudent() {
		tr = ss.beginTransaction();
		String hql = "from com.nishi.Bean.Student";
		Query query = ss.createQuery(hql);
		List<Student> li = query.list();
		if (li.size() > 0) {
			return li;
		}
		return null;
	}

	public List searchByName(String name) {
		Criteria criteria = ss.createCriteria(Student.class);
		criteria.add(Restrictions.ilike("name", "%" + name + "%"));
		List li = criteria.list();
		if (li.size() > 0) {
			return li;
		}
		return null;
	}

}
