package com.nishi.DAO;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Restrictions;

import com.nishi.Bean.Subject;
import com.nishi.HelpingClasses.SessionFact;

public class SubjectOperation implements SubjectDeclaration {

	SessionFactory sf;
	Session ss;
	Transaction tr;

	public SubjectOperation() {
		sf = SessionFact.getSessionFact();
		ss = sf.openSession();
	}

	@Override
	public int insertSubject(Subject sub) {
		tr = ss.beginTransaction();

		int l = (Integer) ss.save(sub);
		tr.commit();
		if (l > 0) {
			return l;
		} else {
			return 0;
		}
	}

	@Override
	public int upDate(int sub_id, Subject sub) {
		tr = ss.beginTransaction();
		sub.setSub_id(sub_id);

		try {
			ss.update(sub);
			tr.commit();
			return sub_id;
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return 0;
	}

	@Override
	public boolean delete(int sub_id) {
		tr = ss.beginTransaction();
		Subject st = new Subject();
		st.setSub_id(sub_id);
		try {
			ss.delete(st);
			tr.commit();
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return false;
	}

	@Override
	public Subject selectSubject(int sub_id) {
		Subject sub = (Subject) ss.get(Subject.class, sub_id);
		if (sub != null) {
			return sub;
		}
		return null;
	}

	@Override
	public List selectAllSubject() {
		tr = ss.beginTransaction();
		String hql = "from com.nishi.Bean.Subject";
		Query query = ss.createQuery(hql);
		List<Subject> li = query.list();
		if (li.size() > 0) {
			return li;
		}
		return null;
	}

	@Override
	public List searchByName(String sub_name) {
		Criteria criteria = ss.createCriteria(Subject.class);
		criteria.add(Restrictions.ilike("sub_name", "%" + sub_name + "%"));
		List li = criteria.list();
		if (li.size() > 0) {
			return li;
		}
		return null;

	}
}
