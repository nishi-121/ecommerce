package com.nishi.Controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.nishi.Bean.Subject;
import com.nishi.DAO.SubjectDeclaration;
import com.nishi.DAO.SubjectOperation;

@WebServlet(name = "AddSubjectController", urlPatterns = { "/AddSubjectController" })
public class AddSubjectController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public AddSubjectController() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String sub_name;
		int sub_passing_marks, sub_max_marks, sub_total_questions, sub_duration, sub_fees, sub_offer;

		sub_name = request.getParameter("sub_name");
		sub_max_marks = Integer.parseInt(request.getParameter("sub_max_marks"));
		sub_passing_marks = Integer.parseInt(request.getParameter("sub_passing_marks"));
		sub_fees = Integer.parseInt(request.getParameter("sub_fees"));
		sub_total_questions = Integer.parseInt(request.getParameter("sub_total_question"));
		sub_duration = Integer.parseInt(request.getParameter("sub_duration"));
		sub_offer = Integer.parseInt(request.getParameter("sub_offer"));

		Subject sub = new Subject(sub_name, sub_passing_marks, sub_max_marks, sub_total_questions, sub_duration,
				sub_fees, sub_offer);
		SubjectDeclaration sd = new SubjectOperation();
		sd.insertSubject(sub);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
