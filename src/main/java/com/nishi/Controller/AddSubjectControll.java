package com.nishi.Controller;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.nishi.Bean.Student;
import com.nishi.Bean.Subject;
import com.nishi.DAO.StudentOperation;
import com.nishi.DAO.SubjectOperation;

public class AddSubjectControll extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public AddSubjectControll() {
		super();

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		SubjectOperation so = new SubjectOperation();
		StudentOperation sto = new StudentOperation();
		Student st = null;
		Subject sub;

		int l = Integer.parseInt(request.getParameter("id"));
		if (request.getParameter("addsub") != null) {
			Enumeration paranames = request.getParameterNames();

			while (paranames.hasMoreElements()) {
				String spname = paranames.nextElement().toString();
				if (!spname.contentEquals("addsub")) {
					if (!spname.contentEquals("id")) {
						response.getWriter().print(spname + " : " + request.getParameter(spname));
						int sub_id = Integer.parseInt(request.getParameter(spname));
						st = sto.selectStudent(l);
						sub = so.selectSubject(sub_id);
						st.getSubjectList().add(sub);

					}
				}
			}
			int returnid = sto.upDate(l, st);
			if (returnid > 0) {
				response.sendRedirect("Update.jsp?id=" + l);
			}
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request, response);
	}

}
