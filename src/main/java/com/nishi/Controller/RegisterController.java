package com.nishi.Controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.nishi.Bean.Student;
import com.nishi.DAO.StudentDeclaration;
import com.nishi.DAO.StudentOperation;
import com.nishi.HelpingClasses.BCrypt;

@WebServlet(name = "RegisterController", urlPatterns = { "/RegisterController" })
public class RegisterController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public RegisterController() {
		super();

	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		PrintWriter out = response.getWriter();
		String name, password, course, address;
		name = request.getParameter("name");
		password = request.getParameter("password");
		password = BCrypt.hashpw(password, BCrypt.gensalt(12));
		address = request.getParameter("address");
		course = request.getParameter("course");
		Student st = new Student(name, password, address, course);
		StudentDeclaration sd = new StudentOperation();
		int id = sd.insertStudent(st);
		if (id > 0) {
			out.println("update successfully");
		} else {
			out.println("there is something wrong.......");
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request, response);
	}

}
