package com.nishi.HelpingClasses;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class SessionFact {
	static SessionFactory sf;

	public static SessionFactory getSessionFact() {
		SessionFactory sf = new Configuration().configure().buildSessionFactory();
		return sf;

	}

}
